from PySide2.QtWidgets import QComboBox, QStyledItemDelegate
from PySide2.QtGui import QStandardItemModel, QStandardItem
from PySide2.QtCore import Qt
from typing import Collection

IS_HEADER = Qt.UserRole
WHITESPACE = ' '


class GroupComboBox(QComboBox):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setModel(QStandardItemModel(self))
        self.setItemDelegate(GroupComboBox.__GroupDelegate(self))

    def addGroup(self, name: str, items: Collection[str]):
        header = QStandardItem(name)
        header.setData(True, IS_HEADER)

        font = header.font()
        font.setBold(True)
        font.setItalic(True)
        header.setFont(font)

        header.setSelectable(False)
        header.setEnabled(False)

        self.model().appendRow(header)

        for item in items:
            q_item = QStandardItem(item)
            q_item.setData(False, IS_HEADER)
            self.model().appendRow(q_item)

    def insertGroup(self, name: str, items: Collection[str], index):
        header = QStandardItem(name)
        header.setData(True, IS_HEADER)

        font = header.font()
        font.setBold(True)
        font.setItalic(True)
        header.setFont(font)

        header.setSelectable(False)
        header.setEnabled(False)

        self.model().insertRow(index, header)

        for i, item in enumerate(items):
            q_item = QStandardItem(item)
            q_item.setData(False, IS_HEADER)
            self.model().insertRow(index + 1 + i, q_item)

    class __GroupDelegate(QStyledItemDelegate):
        def initStyleOption(self, option, index):
            super().initStyleOption(option, index)
            if not index.data(IS_HEADER):
                option.text = 4*WHITESPACE + option.text
