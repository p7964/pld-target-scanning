import matplotlib
matplotlib.use('Qt5Agg')
from PySide2.QtWidgets import QWidget, QVBoxLayout, QSizePolicy
from PySide2.QtCore import Slot
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FCanvasQT
from matplotlib.figure import Figure
import logging
from pld_scan_path import Vertex
from typing import List

X_LIMITS = [-10, 10]
Y_LIMITS = [-10, 10]


class PldScanGraph(QWidget):
    def __init__(self, width=120, height=120, dpi=100):
        super().__init__()
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.figure = Figure()
        self.canvas = FCanvasQT(self.figure)

        width_inch = width / dpi
        height_inch = height / dpi
        self.figure.set_dpi(dpi)
        self.figure.set_figwidth(width_inch)
        self.figure.set_figheight(height_inch)
        self.axes = self.figure.add_axes([0, 0, 1, 1])
        self.axes.set_adjustable("box")
        self.axes.set_xlim(*X_LIMITS)
        self.axes.set_ylim(*Y_LIMITS)
        self.axes.minorticks_on()
        self.axes.grid(linestyle=':', linewidth=1)
        self.axes.tick_params(which='both',
                              direction='in',
                              pad=-10,
                              labelsize=6)
        self.axes.tick_params(axis='y', pad=-10)
        self.axes.set_aspect('equal',
                             anchor='SE')

        lay = QVBoxLayout(self)
        lay.addWidget(self.canvas)

        self.dot = None
        self.contour = None

    def move_dot(self, dot: Vertex):
        if self.dot is not None:
            self.dot.remove()
        if dot is None:
            self.dot = None
        else:
            self.dot = self.axes.plot(dot.x, dot.y, 'g.', markersize = 5)[0]
        self.canvas.draw()

    def draw_contour(self, contour: List[Vertex]):
        if self.contour is not None:
            self.contour.remove()

        x = []
        y = []
        x_0, y_0 = contour[0].xy()

        max_x = x_0
        min_x = x_0
        max_y = y_0
        min_y = y_0

        for v in contour:
            x_, y_ = v.xy()
            x.append(x_)
            y.append(y_)
            if x_ > max_x:
                max_x = x_
            if x_ < min_x:
                min_x = x_
            if y_ > max_y:
                max_y = y_
            if y_ < min_y:
                min_y = y_
        x.append(x_0)
        y.append(y_0)

        d_x = max_x - min_x
        d_y = max_y - min_y
        x_ = (max_x + min_x) / 2
        y_ = (max_y + min_y) / 2

        if d_x == 0 and d_y == 0:
            self.axes.set_xlim(*X_LIMITS)
            self.axes.set_ylim(*Y_LIMITS)
        else:
            d = max(d_x, d_y) / 2 + 1
            self.axes.set_xlim(x_ - d, x_ + d)
            self.axes.set_ylim(y_ - d, y_ + d)

        self.contour = self.axes.plot(x, y, 'r', linewidth=2)[0]
        self.canvas.draw()
