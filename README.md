# PLD Target Scanning



## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.


## Description
Python program allowing for indefinite scanning processes. Backend operates by communicating with Arduino + [Grbl](https://github.com/gnea/grbl). Frontend made with PySide2 allowes for changing relevant Grbl settings, manual stepper positioning and setting up continues operation with a number of available trajectories and parameters.
Designed mainly for moving the focusing lense during Pulse Laser Deposition (PLD) process in order to scan target (material source) with laser beam, hence the name


## Installation
Made and tested with Python 3.7 on Linux (Ubuntu) and Windows 10 (target system), however may also work on any later version of Python 3 and other operating systems. However, it is unlikely to work on a version before Python 3.6, as it heavily relies on f-strings (I think they are neat). All required packages are included in [requirements.txt](requirements.txt)

After unpacking source files into chosen directory, the following line in terminal/cmd will make things operational:
```bash
python -m venv venv             # Create virtual enviroment using venv named venv
./venv/Scripts/activate         # Activate virtual enviroment
pip install -r requirements.txt # Install all required packages
python main.py                  # Start program
```

Also, using virtual enviroment, program could be compiled in executable using pyinstaller.

## Usage
I did my best to make program self-evident to use.

## Support
Feel free to make any fixes to program yourself, as *it is working fine for me*. However, any questions may be addressed to alex.mayzlakh@gmail.com

## Contributing
Please don't make any push/pull/merge requests, just fork and make it your own.

## Authors and acknowledgment
Alexey Mayzlakh, IRE RAS, 2020-2022

## License
[GPL-3.0](https://opensource.org/licenses/GPL-3.0)

## Project status
As stated above, it works for me. However, minor fixes and tweaks may be applied at any moment.
