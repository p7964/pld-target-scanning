import logging
import math
import numpy
import random
from abc import ABC, abstractmethod
from collections import namedtuple

from typing import List


class Vertex:
    """Class for representation of points on a XY plane,
    could be considered as Vectors
    Necessary operations of addition,
    multiplication and comparison are provided"""
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def xy(self):
        return self.x,  self.y

    def __add__(self, other):
        if not isinstance(other, Vertex):
            raise TypeError
        return Vertex(self.x + other.x, self.y + other.y)

    def __radd__(self, other):
        self.__add__(other)

    def __sub__(self, other):
        if not isinstance(other, Vertex):
            raise TypeError(f'{other} is of type {type(other)}')
        return Vertex(self.x - other.x, self.y - other.y)

    def __rsub__(self, other):
        if not isinstance(other, Vertex):
            raise TypeError
        return Vertex(-self.x + other.x, -self.y + other.y)

    def __mul__(self, other):
        if not isinstance(other, (float, int)):
            raise TypeError(f'{other} is of type {type(other)}')
        return Vertex(self.x*other, self.y*other)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False

    def __str__(self):
        return f'Vertex(x={self.x}, y={self.y})'

    def __repr__(self):
        return f'Vertex(x={self.x}, y={self.y})'


class PATH_TYPES:
    """Class containing constants for path type specification
    Text field explains it all"""
    REC_VERT = 'Rectangular vertical'
    REC_HOR = 'Rectangular horizontal'
    Z_VERT = 'Zigzag vertical'
    Z_HOR = 'Zigzag horizontal'

    CLOSED_LOOPS = {REC_VERT, REC_HOR, Z_HOR, Z_VERT}

    IRR_SHORT = 'Irrational short'
    IRR_LONG = 'Irrational long'
    IRR_RAND = 'Irrational random'
    IRR_STEP = 'Irrational by step'

    OPEN_LOOPS = {IRR_STEP, IRR_RAND, IRR_SHORT, IRR_LONG}


def build_rectangle(a: Vertex, b: Vertex):
    return [a, Vertex(a.x, b.y), b, Vertex(b.x, a.y)]

def cross_prod(a: Vertex, b: Vertex, c: Vertex):
    """Cross product of 2d vectors ab and ac
    Not a real cross product, but useful in this specific usage"""
    return (b.x - a.x)*(c.y - a.y) - (c.x - a.x)*(b.y - a.y)


def check_bounds(bounds) -> bool:
    """Functions return true if points of bounds are forming
     correct convex quadrilateral,
    i.e. all angles are less that pi and ab and cd do not cross"""
    [a, b, c, d] = bounds
    return (cross_prod(a,c,d)*cross_prod(a,c,b) < 0
            and cross_prod(b,d,a)*cross_prod(b,d,c) < 0)


def rect_grid(steps):
    """Returns grid of Vertex points, forming square step path
    Number of steps must be at least 2
    Precisely 2 steps are forming perimeter path"""
    if steps < 2:
        raise ValueError('Too few steps for grid formation')

    xx = numpy.linspace(0, 1, steps)
    yy = (0,1)
    grid = []

    for i, x in enumerate(xx[1:]):
        p1 = Vertex(x, yy[(i) % 2])
        p2 = Vertex(x, yy[(i+1) % 2])
        grid.append(p1)
        grid.append(p2)

    for i, x in enumerate(reversed(xx[:-1])):
        p3 = Vertex(x, yy[(i + steps + 1) % 2])
        p4 = Vertex(x, yy[(i + steps) % 2])
        grid.append(p3)
        grid.append(p4)

    return grid


def z_grid(steps):
    """Returns grid of Vertex points, forming triangle, or zigzag, path
    Number of steps must be at least 2
    Precisely 2 steps are forming glass-hour-like path"""

    if steps < 2:
        raise ValueError('Too few steps')

    xx = numpy.linspace(0,1,steps)
    yy = (0,1)
    grid = []

    for i, x in enumerate(xx[1:]):
        grid.append(Vertex(x, yy[(i+1) % 2]))
    grid.append(Vertex(xx[-1], yy[steps % 2]))  # vertical segment at edge

    for i, x in enumerate(reversed(xx[:-1])):
        grid.append(Vertex(x, yy[(steps + i + 1) % 2]))
    grid.append(Vertex(xx[0], yy[0]))   # add vertical segment at the start

    return grid


class PldScanPath:
    """Outer class collects information, check correctness
    and chooses calculator, providing access interface to it"""
    def __init__(self, bounds, path_type, steps=47):
        num_points = len(bounds)

        # Check validity of arguments and raise necessary exceptions

        if num_points != 2 and num_points != 4:
            raise IndexError('Wrong number of vertices')

        if not all(isinstance(point, Vertex) for point in bounds):
            raise TypeError('All elements of bounds should be vertices')

        if steps < 1:
            raise ValueError('Number of steps should be not less than 1')

        # Evaluating bounds vertices

        if num_points == 2:
            [a, b] = bounds
            if a.x == b.x and a.y == b.y:
                raise ValueError('The points are the same')
            elif a.x == b.x or a.y == b.y:
                # Cannot build rectangle or not enough steps
                # for proper scanning, so scanning is done in one line
                self.calc = PldScanPath.LinePath(bounds)
                return

            else:
                # Build points to full rectangle
                bounds = build_rectangle(a, b)

        elif num_points == 4:
            # Correct shape only if ad and bc are crossing
            if not check_bounds(bounds):
                raise ValueError("Incorrect points placement or order")

        # Finally, choose calculator depenging on the path type
        if path_type in PATH_TYPES.CLOSED_LOOPS:
            if steps == 1:
                # Main diagonal line path
                self.calc = PldScanPath.LinePath([bounds[0], bounds[2]])
            elif path_type == PATH_TYPES.REC_HOR:
                self.calc = PldScanPath.RecPathCalc(bounds, steps, False)
            elif path_type == PATH_TYPES.REC_VERT:
                self.calc = PldScanPath.RecPathCalc(bounds, steps, True)
            elif path_type == PATH_TYPES.Z_HOR:
                self.calc = PldScanPath.ZPathCalc(bounds, steps, False)
            elif path_type == PATH_TYPES.Z_VERT:
                self.calc = PldScanPath.ZPathCalc(bounds, steps, True)
            else:
                raise ValueError('Closed type path has no calculator')

                #
                # !!! Important note - horizontal is considered direction
                # along ab vector of 'bounds' list
                # Therefore, all following are considered horizontal
                # and only will be vertical if order of vectors in
                # 'bound' is changed
                #

        elif path_type in PATH_TYPES.OPEN_LOOPS:
            # most steps are irrational and precalculated
            if path_type == PATH_TYPES.IRR_LONG:    # Long irrational step
                self.calc = PldScanPath.IrrPathCalc(bounds,
                                                    PldScanPath.IrrPathCalc.IRR_STEP_LONG)
            elif path_type == PATH_TYPES.IRR_SHORT: # Short irrational step
                self.calc = PldScanPath.IrrPathCalc(bounds,
                                                    PldScanPath.IrrPathCalc.IRR_STEP_SHORT)
            elif path_type == PATH_TYPES.IRR_RAND:  # Random irrational step
                self.calc = PldScanPath.IrrPathCalc(bounds,
                                                    PldScanPath.IrrPathCalc.IRR_STEP_RAND)
            elif path_type == PATH_TYPES.IRR_STEP:  # Irrational steps
                                                    # is calculated based on 'steps' parameter
                self.calc = PldScanPath.IrrPathCalc(bounds,
                                                    PldScanPath.IrrPathCalc.IRR_STEP_USER(steps))
            else:
                raise ValueError('Open type path has no calculator')

    def next_point(self):
        # Call to calculator to calc next point and return already stored point
        # First point, in most cases, is 'a' vector
        return self.calc.next_point()

    class AbstractPathCalc(ABC):
        """Abstract class for all calculators.
        Common functionality defined here"""
        def __init__(self, borders: List[Vertex],
                     steps: int, orientation: bool):
            self.borders = borders
            self.steps = steps
            self.orientation = orientation  # False means moving along x axis
                                            # True means moving along y axis
                                            # Only applicable to closed loops,
                                            # but perhaps should be
                                            # considered for all?
            self.next = Vertex(0, 0)    # start at zero, which will be
                                        # recalculated to first
                                        # vector of 'borders'

        @abstractmethod
        def next_point(self):
            """Define here main function of calculator. All iterations goes here"""
            pass

        def order_xy(self, point):
            """Orders vertex coordinates depending on preset orientation"""
            if self.orientation:
                return point.y, point.x
            else:
                return point.x, point.y

        def scale_to_borders(self, x, y) -> Vertex:
            """From (0,1) range, used for inner calculations, scale to points to given border
            Perhaps could be done in outer class?"""
            [a, b, c, d] = self.borders
            return a + x*(b - a) + y*(d - a) + y*x*(c + a - b - d)

    class RecPathCalc(AbstractPathCalc):
        def __init__(self, borders, steps, orientation):
            super(PldScanPath.RecPathCalc, self).__init__(borders,
                                                          steps, orientation)
            self.index = 0  # index needed for iterating grid
            self.grid = rect_grid(steps)    # calculate grid
            self.grid_size = len(self.grid)

        def next_point(self):
            point = self.next
            x, y = self.order_xy(
                self.grid[self.index]
            )
            self.index = (self.index + 1) % self.grid_size  # to loop the grid
            self.next = Vertex(x, y)
            return self.scale_to_borders(point.x, point.y)

    class ZPathCalc(AbstractPathCalc):
        def __init__(self, borders, steps, orientation):
            super(PldScanPath.ZPathCalc, self).__init__(
                borders, steps, orientation)
            self.index = 0   # index needed for iterating grid
            self.grid = z_grid(steps)
            self.grid_size = len(self.grid)

        def next_point(self):
            point = self.next
            x, y = self.order_xy(
                self.grid[self.index]
            )
            self.index = (self.index + 1) % self.grid_size
            self.next = Vertex(x, y)
            return self.scale_to_borders(point.x, point.y)

    class IrrPathCalc(AbstractPathCalc):
        """Calculated steps chosen arbitrary based
        on visual evaluation of perfomance in given tasks"""
        IRR_STEP_SHORT = (math.sqrt(5)-1) / math.sqrt(83)
        IRR_STEP_LONG = math.sin(math.sqrt(10)/9*math.pi)
        IRR_STEP_RAND = (math.pi + random.random()) % 1
        IRR_STEP_USER = lambda x: (x * math.sqrt(31)) % 1

        def __init__(self, bounds, param, steps=0):
            super(PldScanPath.IrrPathCalc, self).__init__(bounds, steps, False)
            self.delta = param
            self.up_down_flag = random.choice([True, False])     # False if d -> u, True if u -> d
            self.left_right_flag = random.choice([False, True])  # False if r -> l, True if l -> r
            self.bounce_flag = False
            self.next = Vertex(random.random(), float(self.up_down_flag))   # first vertex is random point on one of
                                                                            # the edges, parallel to x

        def next_point(self):
            next_point = self.next
            if self.bounce_flag:
                # bounced of the vertical line, need to calculate x position
                # based on current y
                self.bounce_flag = False
                self.up_down_flag = not self.up_down_flag
                if self.up_down_flag:
                    y = 1.0
                    a = (1.0 - next_point.y)
                else:
                    y = 0.0
                    a = next_point.y
                if self.left_right_flag:
                    x = self.delta * a
                else:
                    x = 1.0 - self.delta * a
            else:
                # if moving left to right, add, otherwise substract
                if self.left_right_flag:
                    x = next_point.x + self.delta
                else:
                    x = next_point.x - self.delta

                if 0 < x < 1:
                    # if in bounds
                    self.up_down_flag = not self.up_down_flag   # up-down flip
                    y = float(self.up_down_flag)    # always on upper or lower edge
                else:
                    self.bounce_flag = True
                    # Following is based on geometry calculations and takes into account movement directions
                    if self.left_right_flag:
                        a = (1 - next_point.x) / self.delta
                    else:
                        a = next_point.x / self.delta
                    x = float(self.left_right_flag)
                    self.left_right_flag = not self.left_right_flag
                    if self.up_down_flag:
                        y = 1 - a
                    else:
                        y = a

            self.next = Vertex(x, y)
            return self.scale_to_borders(next_point.x, next_point.y)

    class LinePath:
        def __init__(self, bounds):
            if len(bounds) != 2:
                raise ValueError('Num of points is not equal 2')
            self.a = bounds[0]
            self.b = bounds[1]
            self.next = self.a

        def next_point(self):
            next_point = self.next
            if next_point == self.a:
                self.next = self.b
            elif next_point == self.b:
                self.next = self.a
            else:
                if isinstance(self.next, Vertex):
                    raise ValueError(
                        'Unexpected third point in single line path'
                    )
                else:
                    raise TypeError(
                        'Wrong type of value stored in one of the fields'
                    )
            return next_point

