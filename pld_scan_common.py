from PySide2.QtWidgets import QDoubleSpinBox
from typing import NamedTuple
from pld_scan_path import Vertex


UPDATE_PERIOD = .2


def get_ds_box():
    box = QDoubleSpinBox()
    box.setKeyboardTracking(False)
    box.setRange(-15, 15)
    box.setSuffix(' mm')
    box.setDecimals(1)
    box.setSingleStep(.1)
    return box


class JobTuple(NamedTuple):
    vertex: Vertex
    speed: float


class UpdateTuple(NamedTuple):
    x: float
    y: float
    status: str
    error: str