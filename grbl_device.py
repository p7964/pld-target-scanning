from serial import Serial, SerialException
import serial.tools.list_ports as stl
import threading, time, re
import logging
from collections import namedtuple

from typing import Dict, NamedTuple


# grbl_ports = namedtuple('grbl_ports', ['name', 'g_ver'])
class GrblPort(NamedTuple):
    port_name: str
    grbl_version: str


# grbl_state = namedtuple('grbl_state', ['x', 'y', 'status'])
class GrblState(NamedTuple):
    x: float
    y: float
    status: str


def serial_ports():
    logging.debug('Scanning ports')
    temp_ports = stl.comports()
    ports = []
    query = []
    for port in temp_ports:
        try:
            logging.debug(f'Trying port {port.name}')
            s = Serial(port.device, baudrate=115200, timeout=.5)
            query.append(s)
        except SerialException:
            logging.error(f'Exception on {port.name}')
            pass
    time.sleep(2)
    logging.debug('Formatting ports')
    for s in query:
        grbl_version = 'n/a'
        try:
            resp = s.readall().decode('UTF-8')
            if 'Grbl' in resp:
                b1 = resp.split('Grbl ')
                b2 = b1[1].split(' [')
                grbl_version = b2[0]
        except SerialException:
            pass
        finally:
            s.close()
            logging.debug(f'Port {s.port} closed, Grbl is {grbl_version}')
        ports.append(GrblPort(port_name=s.port, grbl_version=grbl_version))
    logging.debug(f'Scanned {len(ports)} ports')
    return sorted(ports, key=lambda x: (x.grbl_version, x.port_name))


def parse_status(resp_bytes: bytes):
    x = None
    y = None
    status = None
    try:
        resp_str = resp_bytes.decode('UTF-8')
        _split = re.split(r'[,:|<>]', resp_str)
        status = _split[1]
        if 'Hold' in status:
            logging.debug(resp_str)
        # mpos_index = _split.index('MPos')
        wpos_index = _split.index('WPos')
        if 'Pos' not in _split[2]:
            status += ':' + _split[2]
        x = float(_split[wpos_index + 1])
        y = float(_split[wpos_index + 2])
    except (UnicodeDecodeError, AttributeError, IndexError) as err:
        logging.error(
            'Status parsing error: %s\nResponse was %s',
            err,
            resp_bytes
        )
    finally:
        return GrblState(x, y, status)


def parse_params(resp_arr):
    conf_dict = {}
    for line in resp_arr:
        key, value = line.split('=')
        if key in CONF_KEY:
            conf_dict[key] = value.split(' ')[0]
    return conf_dict


def parse_err(err_resp):
    return 'error'  # it really is a placeholder


CYCLE_SLEEP_TIME = .25
WAIT_RESP_TIME = .025


WPOS_MASK_V09 = 0b10
WPOS_MASK_V11 = 0b0


GRBL_HOLD = b'!'
GRBL_RESUME = b'~'
GRBL_RESET = b'\x18'
GRBL_QUERY = b'?'
GRBL_CONF = b'$$\n'


CONF_KEY = {'$0': 'Step pulse, us',
            '$1': 'Step idle delay, ms',
            '$2': 'Step port invert',
            '$3': 'Direction port invert',
            '$4': 'Step enable invert',
            '$5': 'Limit pins invert',
            '$6': 'Probe pin invert',
            '$10': 'Status report',
            '$11': 'Junction deviation, mm',
            '$12': 'Arc tolerance, mm',
            '$13': 'Report inches',
            '$20': 'Soft limits',
            '$21': 'Hard limits',
            '$22': 'Homing cycle',
            '$23': 'Homing direction invert',
            '$24': 'Homing feed, mm/min',
            '$25': 'Homing seek, mm/min',
            '$26': 'Homing debounce, ms',
            '$27': 'Homing pull-off, mm',
            '$30': 'Max spindle speed, RPM',
            '$31': 'Min spindle speed, RPM',
            '$32': 'Laser mode',
            '$100': 'X resolution, steps/mm',
            '$101': 'Y resolution, steps/mm',
            '$102': 'Z resolution, steps/mm',
            '$110': 'X Max rate, mm/min',
            '$111': 'Y Max rate, mm/min',
            '$112': 'Z Max rate, mm/min',
            '$120': 'X-axis acceleration, mm/sec^2',
            '$121': 'Y-axis acceleration, mm/sec^2',
            '$122': 'Z-axis acceleration, mm/sec^2',
            '$130': 'X Max travel, mm',
            '$131': 'Y Max travel, mm',
            '$132': 'Z Max travel, mm'
            }

EXPIRED_ERROR_MSG = 'This instance has expired! Start a new one'


class GrblDevice(Serial):
    def __init__(self, port: str):
        self.grbl_error = ''
        self.grbl_lock = threading.RLock()
        self.grbl_on = True
        self.config = {}

        try:
            logging.debug(f'Port is "{port}"')
            super().__init__(port=port, baudrate=115200, timeout=.5)
            time.sleep(2)   # waiting for Grbl response
            resp = self.readall()
            if b'Grbl' not in resp:
                raise SerialException("Not a Grbl device!")
        except SerialException as err:
            logging.error('Error opening Grbl')
            self.grbl_on = False
            raise err

        self.get_config()
        report_mask = int(self.config['$10'])
        wpos_mask = WPOS_MASK_V09 if b'0.9' in resp else WPOS_MASK_V11
        if not report_mask & wpos_mask:
            report_mask += wpos_mask
            self.save_config({'$10':str(report_mask)})

        self.state = self.grbl_position()
        logging.debug(self.state)

        self.query_thread = threading.Thread(target=self.__query_cycle,
                                             name='Grbl Query')
        self.query_thread.start()

        logging.debug('Grbl connected and started')
        
    def open(self):
        if self.grbl_on:
            super(GrblDevice, self).open()
        else:
            raise SerialException(EXPIRED_ERROR_MSG)

    def grbl_position(self) -> GrblState:
        """Query Grbl position and status"""
        with self.grbl_lock:
            try:
                self.write(GRBL_QUERY)
                time.sleep(WAIT_RESP_TIME)
                resp = self.readline()
                if b'Grbl' in resp:
                    raise SerialException
                state = parse_status(resp)
                if state.x is None or state.y is None:
                    state = GrblState(
                        self.state.x,
                        self.state.y,
                        state.status
                    )
                return state
            except SerialException:
                self.ser_err_handle()

    def __query_cycle(self):
        while self.grbl_on:
            self.state = self.grbl_position()
            time.sleep(CYCLE_SLEEP_TIME)

    def ser_err_handle(self):
        logging.error('Error with Grbl device!')
        if not self.grbl_on:
            raise SerialException(EXPIRED_ERROR_MSG)
        try:
            with self.grbl_lock:
                self.close()
                time.sleep(.5)
                self.open()
                time.sleep(2)
                resp = self.readall()
                if b'Grbl' not in resp:
                    raise SerialException
                x = self.state.x
                y = self.state.y
                self.correct_position(x, y)
                logging.info('Reconnection successful')
        except SerialException:
            logging.error('Failed to reconnect to Grbl! Shutting down device')
            self.grbl_on = False
            self.grbl_error = 'Serial Error: disconnected'
            self.close()
            raise SerialException

    def get_config(self):
        with self.grbl_lock:
            try:
                self.write(GRBL_CONF)
                time.sleep(WAIT_RESP_TIME)
                lines = []
                while True:
                    resp = self.readline().decode('UTF-8')
                    if ('ok' in resp) or (resp is None) or ('error' in resp):
                        self.grbl_error = resp[:-1]
                        break
                    elif 'Grbl' in resp:
                        raise SerialException
                    else:
                        lines.append(resp)
                config = parse_params(lines)
                self.config = config
                return config
            except SerialException:
                self.ser_err_handle()

    def save_config(self, new_config: Dict[str, str]):
        with self.grbl_lock:
            change_flag = False
            if not isinstance(new_config, dict):
                raise TypeError('Config should be a dict')
            for key, value in new_config.items():
                if key not in CONF_KEY:
                    raise ValueError(f'Key {key} is not a valid parameter')
                elif value != self.config[key]:
                    change_flag = True
                    with self.grbl_lock:
                        data = key + '=' + value
                        try:
                            self.write((data + '\n').encode('UTF-8'))
                            self.eval_resp()
                            if 'ok' in self.grbl_error:
                                logging.info(f'"{data}" saved to Grbl')
                            else:
                                logging.error(f'Failed to save "{data}" to Grbl')
                        except SerialException:
                            self.ser_err_handle()
            if change_flag:
                self.get_config()

    def send_g0(self, x: float, y: float):
        msg = b"G0 X%.2f Y%.2f\n" % (x, y)
        with self.grbl_lock:
            try:
                self.write(msg)
                time.sleep(WAIT_RESP_TIME)
                self.eval_resp()
                logging.debug('Command ' + msg.decode('UTF-8') + ' sent')
            except SerialException as err:
                raise err

    def correct_position(self, x: float, y: float):
        msg = b'G92 X%.2f Y%.2f\n' % (x, y)
        with self.grbl_lock:
            try:
                self.write(msg)
                time.sleep(WAIT_RESP_TIME)
                self.eval_resp()
                logging.debug(f'Position corrected to x={x}, y={y}')
            except SerialException:
                self.ser_err_handle()

    def send_g1(self, x: float, y: float, speed: float):
        msg = b"G01 X%.1f Y%.1f F%.1f\n" % (x, y, speed)
        with self.grbl_lock:
            try:
                self.write(msg)
                time.sleep(WAIT_RESP_TIME)
                self.eval_resp()
                logging.debug('Command ' + msg.decode('UTF-8') + ' sent')
            except SerialException:
                self.ser_err_handle()

    def eval_resp(self, resp: str=None):
        if resp is None:
            resp = self.readline().decode('UTF-8')
        self.grbl_error = resp[:-1]
        if (
                (
                        'ok' not in resp and 'error' not in resp
                )
                or 'Grbl' in resp
        ):
            raise SerialException
        else:
            return True

    def hold(self) -> bool:
        status = self.state.status
        if 'Hold' not in status and 'Idle' not in status:
            logging.debug('Holding Grbl')
            # self.oper_lock.acquire()
            with self.grbl_lock:
                try:
                    self.write(GRBL_HOLD)
                    time.sleep(WAIT_RESP_TIME)
                    logging.debug('Grbl holt')
                except SerialException:
                    self.ser_err_handle()
            return True
        else: return False

    def resume(self):
        status = self.state.status
        if 'Hold' in status:
            logging.debug('Resuming Grbl')
            with self.grbl_lock:
                try:
                    self.write(GRBL_RESUME)
                    time.sleep(WAIT_RESP_TIME)
                    logging.debug('Grbl resumed')
                except SerialException:
                    self.ser_err_handle()
            return True
        else: return False

    def stop(self):
        logging.debug('Stopping Grbl')
        with self.grbl_lock:
            try:
                status = self.state.status
                if 'Idle' not in status:
                    if 'Hold' not in status:
                        self.hold()
                        time.sleep(.5)
                        while 'Hold' not in self.state.status:
                            logging.debug(self.state.status)
                            time.sleep(.5)
                            self.state = self.grbl_position()
                    self.write(GRBL_RESET)
                    resp = self.readall()
                    if b'Grbl' not in resp:
                        raise SerialException
            except SerialException:
                self.ser_err_handle()
            # except AttributeError:
            #     self.state = GrblState(x=0, y=0, status='Off')
            #     return
        logging.debug('Grbl stopped')

    def reset(self):
        logging.debug('Sending Grbl to (0, 0)')
        self.stop()
        state = self.grbl_position()
        if state.x != 0 or state.y != 0:
            self.send_g0(0, 0)

    def disconnect(self):
        logging.debug('Disconnectiong Grbl')
        self.reset()
        while self.state.x != 0 or self.state.y != 0:
            time.sleep(1)
        logging.debug('Zero reached')
        self.grbl_on = False
        self.query_thread.join(timeout=2)
        self.close()
        
    def __del__(self):
        if self.grbl_on:
            self.disconnect()
        super().__del__()
