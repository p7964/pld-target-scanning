import logging
from typing import List

from PySide2.QtWidgets import (QGroupBox, QPushButton, QComboBox, QHBoxLayout)
from PySide2.QtCore import Signal, Slot, QThread, QObject

from pld_scan_config_window import PldScanConfigWindow
from grbl_device import GrblPort


class DeviceUI(QGroupBox):
    open_signal = Signal()
    find_devices = Signal()

    device_list: List[GrblPort] = []

    def __init__(self, parent):
        super().__init__('Device', parent)

        self.refresh_button = QPushButton('Refresh')

        self.device_list_box = QComboBox()
        self.refresh_button.clicked.connect(self.find_devices.emit)

        self.connect_button = QPushButton('Connect')
        self.connect_button.clicked.connect(self.open_signal.emit)

        self.config_button = QPushButton('Config')
        self.config_button.clicked.connect(self.open_config)

        layout = QHBoxLayout()
        layout.addWidget(self.refresh_button)
        layout.addWidget(self.device_list_box)
        layout.addWidget(self.connect_button)
        layout.addWidget(self.config_button)
        self.setLayout(layout)

        self.fd_thread = None
        self.fd_worker = None

    @Slot()
    def worker_start(self):
        logging.debug('Got started signal')
        self.device_list_box.clear()
        self.setDisabled(True)

    @Slot(list)
    def worker_finished(self, new_device_list: List[GrblPort]):
        logging.debug(f'Got finished signal, len={len(new_device_list)}')
        self.device_list = new_device_list
        self.device_list_box.addItems(
            [f'{item.port_name} ({item.grbl_version})' for item in new_device_list]
        )
        self.setEnabled(True)

    # def open_close(self):
    #     index = self.device_list_box.currentIndex()
    #     port = self.device_list[index].name
    #     logging.debug(type(port))
    #     self.open_signal.emit(port)

    def open_config(self):
        conf_window = PldScanConfigWindow(self.parent(),
                                          self.parent().grbl_object)
        conf_window.exec_()

    def state_open(self):
        self.connect_button.setText('Disconnect')
        self.device_list_box.setDisabled(True)
        self.refresh_button.setDisabled(True)
        self.config_button.setEnabled(True)

    def state_closed(self):
        self.device_list_box.setEnabled(True)
        self.refresh_button.setEnabled(True)
        self.connect_button.setText('Connect')
        self.config_button.setDisabled(True)

    def state_running(self):
        self.connect_button.setText('Disconnect')
        self.device_list_box.setDisabled(True)
        self.refresh_button.setDisabled(True)
        self.config_button.setEnabled(False)