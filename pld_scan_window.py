import logging

from PySide2.QtWidgets import (
    QWidget,
    QHBoxLayout,
    QVBoxLayout,
    QPushButton,
)
from PySide2.QtCore import QThread


from pld_scan_task_ui import TaskParametersUI
from pld_scan_device_ui import DeviceUI
from pld_scan_monitoring_ui import MonitoringUI
from pld_scan_workers import *


class PldScanWindow(QWidget):
    grbl_object: GrblDevice = None

    """Thread and worker objects variables"""
    fd_thread: QThread = None
    update_thread: QThread = None
    open_close_thread: QThread = None

    fd_worker: FindDevicesWorker = None
    update_worker: UpdateWorker = None
    open_worker: GrblOpenWorker = None
    close_worker: GrblCloseWorker = None
    job_worker: JobWorker = None

    close_signal = Signal()

    def __init__(self):
        super(PldScanWindow, self).__init__()
        self.setWindowTitle('Steppers control')

        self.close_signal.connect(self.close)
        self.close_signal.connect(lambda: logging.debug('Closing signal sent'))

        """Load UI elements and connect their button to methods"""

        self.device_UI = DeviceUI(self)
        self.device_UI.find_devices.connect(self.find_devices)
        self.device_UI.connect_button.clicked.connect(self.grbl_oc)
        self.tasks_UI = TaskParametersUI(self)
        self.tasks_UI.draw_contour.connect(self.draw_contour)
        self.monitoring_UI = MonitoringUI(self)
        self.monitoring_UI.set_XY.connect(self.set_XY)

        """Local buttons and their connections"""

        self.start_button = QPushButton('Start')
        self.start_button.setEnabled(False)
        self.start_button.clicked.connect(self.start_job)

        self.pause_button = QPushButton('Pause')
        self.pause_button.clicked.connect(self.pause)
        self.pause_button.setEnabled(False)

        self.stop_button = QPushButton('Stop')
        self.stop_button.setEnabled(False)
        self.stop_button.clicked.connect(self.stop)

        self.reset_button = QPushButton("Reset")
        self.reset_button.setEnabled(False)
        self.reset_button.clicked.connect(self.reset)

        """
        Compiling layout
        """
        layout = QVBoxLayout()
        mid_layout = QHBoxLayout()
        layout.addWidget(self.device_UI)
        mid_layout.addWidget(self.tasks_UI)
        mid_layout.addStretch()
        mid_layout.addWidget(self.monitoring_UI)
        layout.addLayout(mid_layout)
        layout.addStretch()

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.start_button)
        button_layout.addWidget(self.pause_button)
        button_layout.addWidget(self.stop_button)
        button_layout.addWidget(self.reset_button)

        layout.addLayout(button_layout)

        self.setLayout(layout)
        """
        Finishing touches
        """
        self.state_closed()
        self.show()
        """
        Starting with populating device list and box
        """
        self.find_devices()

    def closeEvent(self, event):
        """Defining closing sequence"""
        logging.debug('Shutdown event')
        if self.grbl_object is not None and self.grbl_object.grbl_on:
            logging.debug('Grbl is connected, ignoring event until disconnect')
            event.ignore()
            self.setEnabled(False)
            self.grbl_object.disconnect() # If Grbl connected, first disconnect
            return
        """
        Check on all remaining threads and terminate if necessary
        """
        for thread in (self.fd_thread,
                       self.open_close_thread,
                       self.update_thread):
            if thread is not None and thread.isRunning():
                logging.debug(f'Waiting for {thread.objectName()} thread')
                if not thread.wait(3000):
                    logging.debug("Time's up!")
                    thread.terminate()
        logging.debug('accepting close event')
        event.accept()
        logging.debug('have a nice day')

    """
    Following methods define UI states of enabled elements
    """

    def state_open(self):
        self.device_UI.state_open()
        self.monitoring_UI.state_opened()
        self.tasks_UI.state_open()
        self.start_button.setEnabled(True)
        self.stop_button.setEnabled(True)
        self.pause_button.setEnabled(True)
        self.reset_button.setEnabled(True)

    def state_closed(self):
        self.device_UI.state_closed()
        self.monitoring_UI.state_closed()
        self.tasks_UI.state_closed()
        self.start_button.setEnabled(False)
        self.pause_button.setEnabled(False)
        self.stop_button.setEnabled(False)
        self.reset_button.setEnabled(False)

    def state_running(self):
        self.device_UI.state_running()
        self.monitoring_UI.state_running()
        self.tasks_UI.state_running()
        self.start_button.setEnabled(False)

    """Service methods"""

    def find_devices(self):
        """Refresh define list and freeze UI in the meantime"""
        self.fd_thread = QThread()
        self.fd_thread.setObjectName('Find Devices thread')
        self.fd_worker = FindDevicesWorker()

        self.fd_worker.started.connect(self.device_UI.worker_start)
        self.fd_worker.finished.connect(self.device_UI.worker_finished)

        self.fd_worker.moveToThread(self.fd_thread)
        self.fd_worker.finished.connect(self.fd_thread.quit)
        self.fd_thread.started.connect(self.fd_worker.run)
        self.fd_thread.start()

    @Slot()
    def grbl_oc(self):
        """Actions when connect/disconnect button clicked"""
        index = self.device_UI.device_list_box.currentIndex()
        port = self.device_UI.device_list[index].port_name
        logging.debug(f'Connect button clicked, port {port}')
        self.setEnabled(False)  # Disable UI until task is done
        if self.grbl_object is not None and self.grbl_object.grbl_on:
            logging.debug('Disconnectiong')
            self.grbl_disconnect()
        else:
            logging.debug('Connecting')
            self.grbl_connect(port)

    def grbl_connect(self, port):
        """Start thread for connecting to device"""
        self.open_worker = GrblOpenWorker(port)
        self.open_close_thread = QThread()
        self.open_close_thread.setObjectName("Open/Close thread")
        self.open_worker.finished.connect(self.__grbl_opened)
        self.open_worker.failure.connect(self.__grbl_closed)
        self.open_worker.moveToThread(self.open_close_thread)
        self.open_close_thread.started.connect(self.open_worker.run)
        self.open_worker.failure.connect(self.open_close_thread.quit)
        self.open_worker.finished.connect(self.open_close_thread.quit)
        logging.debug('Starting thread')
        self.open_close_thread.start()

    def grbl_disconnect(self):
        """Start thread for disconnecting from the device"""
        self.close_worker = GrblCloseWorker(self.grbl_object)
        self.open_close_thread = QThread()
        self.open_close_thread.setObjectName("Open/Close thread")
        self.close_worker.moveToThread(self.open_close_thread)
        self.open_close_thread.started.connect(self.close_worker.run)
        self.close_worker.finished.connect(self.open_close_thread.quit)
        logging.debug('Starting thread')
        self.open_close_thread.start()

    @Slot(GrblDevice)
    def __grbl_opened(self, grbl_object):
        """Called when device connected, starting update thread"""
        self.setEnabled(True)   # enable UI
        logging.debug('Starting update thread')
        self.grbl_object = grbl_object
        self.update_worker = UpdateWorker(self.grbl_object)
        self.update_worker.update.connect(
            self.monitoring_UI.grbl_update
        )
        self.update_thread = QThread()  # it's crude but it is working
        self.update_thread.setObjectName('Update thread')
        self.update_worker.moveToThread(self.update_thread)
        self.update_worker.finished.connect(self.update_thread.quit)
        self.update_worker.finished.connect(
            lambda: logging.debug('Finished signal from UpdateWorker received')
        )
        self.update_worker.finished.connect(self.__grbl_closed)
        self.update_thread.started.connect(self.update_worker.run)
        self.update_thread.start()
        self.state_open()

    @Slot()
    def __grbl_closed(self):
        """Called when device disconnected"""
        logging.debug('Grbl closed, clearing')
        self.update_thread.wait()
        self.setEnabled(True)
        # self.grbl_object = None
        self.state_closed()

    def draw_contour(self, contour):
        """Passing signal to draw workspace bounds to plotting element"""
        self.monitoring_UI.graph.draw_contour(contour)

    def set_XY(self):
        """Called when value changed in boxes in monitoring UI elements.
        Getting values of x and y from UI boxes and sending them to Grbl"""
        x = self.monitoring_UI.x_box.value()
        y = self.monitoring_UI.y_box.value()
        if self.job_worker is not None:
            return  # Do not send to Grbl if job is running
        else:
            if self.grbl_object is not None and self.grbl_object.grbl_on:
                self.grbl_object.send_g0(x, y)

    def start_job(self):
        """Get job parameters from task UI and start scanning"""
        if self.update_thread is None or not self.update_thread.isRunning():
            return  # update thread should be running
        path_type, speed, steps, bounds = self.tasks_UI.get_contents()
        try:
            state = self.grbl_object.state
            starting_point = Vertex(state.x, state.y)
            self.job_worker = JobWorker(path_type, bounds, steps, speed,
                                        starting_point)
        except Exception as err:    # What could possibly go wrong?
            logging.error(err)
            self.tasks_UI.path_error_label.setText(str(err))
            return
        self.tasks_UI.path_error_label.setText('')
        self.state_running()    # Change UI state
        """
        Job worker should listen to updates and send next point when
        Grbl is close to the last point sent
        """
        self.job_worker.next_signal.connect(self.next_point)
        self.update_worker.update.connect(self.job_worker.update)
        self.job_worker.send_next() # Start by sending first point

    @Slot(JobTuple)
    def next_point(self, job_tuple: JobTuple) -> None:
        """Recieve next point and send it to Grbl"""
        v = job_tuple.vertex
        s = job_tuple.speed
        if self.grbl_object is not None and self.grbl_object.grbl_on:
            self.grbl_object.send_g1(v.x, v.y, s)

    def stop(self):
        """Stop current task and movement"""
        if self.grbl_object is not None and self.grbl_object.grbl_on:
            if self.job_worker is not None:
                logging.debug('Stopping worker')
                self.update_worker.update.disconnect(self.job_worker.update)
                self.job_worker = None
            self.grbl_object.stop()
            # If not called during job,
            # set boxes to current position without sending signal
            self.monitoring_UI.set_XY.disconnect(self.set_XY)
            state = self.grbl_object.state
            x = state.x
            y = state.y
            self.monitoring_UI.x_box.setValue(x)
            self.monitoring_UI.y_box.setValue(y)
            self.monitoring_UI.set_XY.connect(self.set_XY)
            self.state_open()

    def reset(self):
        """Perform stop and send to (0,0). Also reset boxes w/o signal"""
        if self.grbl_object is not None and self.grbl_object.grbl_on:

            self.stop()
            self.monitoring_UI.set_XY.disconnect(self.set_XY)
            self.monitoring_UI.x_box.setValue(0)
            self.monitoring_UI.y_box.setValue(0)
            self.grbl_object.reset()
            self.monitoring_UI.set_XY.connect(self.set_XY)

    def pause(self):
        """Pause current Grbl movement, do not stop or reset Grbl"""
        if self.grbl_object is not None and self.grbl_object.grbl_on:
            if self.pause_button.text() == 'Pause':
                if self.grbl_object.hold():
                    self.pause_button.setText('Resume')
            elif self.pause_button.text() == 'Resume':
                if self.grbl_object.resume():
                    self.pause_button.setText('Pause')
