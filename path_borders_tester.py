import unittest
from pld_scan_path import Vertex, check_bounds


class TestPathBorders(unittest.TestCase):
    def test_correct_orientation(self):
        a = Vertex(0, 0)
        b = Vertex(0, 1)
        c = Vertex(1, 1)
        d = Vertex(1, 0)
        cases = ([a,b,c,d],
                 [b,c,d,a],
                 [c,d,a,b],
                 [d,a,b,c],
                 [a,d,c,b],
                 [d,c,b,a],
                 [c,b,a,d],
                 [b,a,d,c])
        for case in cases:
            with self.subTest(x=case):
                self.assertTrue(check_bounds(case))

        b = Vertex(2, 1)
        c = Vertex(1, 3)
        d = Vertex(-1, 2)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertTrue(check_bounds(case))

        a = Vertex(5,-1)
        b = Vertex(6, -2)
        c = Vertex(5, -3)
        d = Vertex(4, -2)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertTrue(check_bounds(case))

    def test_wrong_orientation(self):
        a = Vertex(0, 0)
        c = Vertex(0, 1)
        b = Vertex(1, 1)
        d = Vertex(1, 0)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

        c = Vertex(2, 1)
        b = Vertex(1, 3)
        d = Vertex(-1, 2)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

        a = Vertex(5, -1)
        c = Vertex(6, -2)
        b = Vertex(5, -3)
        d = Vertex(4, -2)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

    def test_doubling_points(self):
        a = Vertex(0, 0)
        b = Vertex(0, 0)
        c = Vertex(1, 1)
        d = Vertex(1, 0)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

        b = Vertex(2, 1)
        c = Vertex(0, 0)
        d = Vertex(-1, 2)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

        a = Vertex(5, -1)
        b = Vertex(6, -2)
        c = Vertex(5, -3)
        d = Vertex(5, -1)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

    def test_convex_contour(self):
        a = Vertex(0, 0)
        b = Vertex(1, 2)
        c = Vertex(3, 3)
        d = Vertex(0, 3)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                # print(check_bounds(case))
                self.assertFalse(check_bounds(case))

        b = Vertex(0, 1)
        c = Vertex(-1, 2)
        d = Vertex(1, 3)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

        a = Vertex(4, 4)
        b = Vertex(3, 6)
        c = Vertex(4, 5)
        d = Vertex(5, 6)
        cases = ([a, b, c, d],
                 [b, c, d, a],
                 [c, d, a, b],
                 [d, a, b, c],
                 [a, d, c, b],
                 [d, c, b, a],
                 [c, b, a, d],
                 [b, a, d, c])
        for case in cases:
            with self.subTest(x=case):
                self.assertFalse(check_bounds(case))

if __name__ == "__main__":
    log = 'border_test_log.txt'
    with open(log, 'w') as f:
        runner = unittest.TextTestRunner(f)
        unittest.main(testRunner=runner)