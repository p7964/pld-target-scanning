import logging
import time
from typing import List

from PySide2.QtCore import QObject, Signal, Slot

from pld_scan_common import UPDATE_PERIOD, UpdateTuple, JobTuple
from grbl_device import GrblDevice, serial_ports
from pld_scan_path import Vertex, PldScanPath


class UpdateWorker(QObject):
    update = Signal(dict)
    finished = Signal()
    FINISHED_UPDATE = UpdateTuple(x=None,
                                  y=None,
                                  status='Off',
                                  error='None')

    def __init__(self, grbl_device: GrblDevice):
        super().__init__()
        self.grbl_device = grbl_device
        self.state = None

    def run(self):
        while self.grbl_device.grbl_on:
            time.sleep(UPDATE_PERIOD)
            state = self.grbl_device.state
            error = self.grbl_device.grbl_error
            x = state.x
            y = state.y
            status = state.status
            self.state = state

            update = UpdateTuple(x, y, status, error)
            self.update.emit(update)
        self.update.emit(self.FINISHED_UPDATE)
        logging.debug('Closing update thread')
        self.finished.emit()


class GrblOpenWorker(QObject):
    failure = Signal()
    finished = Signal(GrblDevice)

    def __init__(self, port: str):
        self.port = port
        super().__init__()

    def run(self):
        logging.debug('Running open')
        grbl_object = GrblDevice(self.port)
        if grbl_object is not None and grbl_object.grbl_on:
            logging.debug('Grbl Opened')
            self.finished.emit(grbl_object)
        else:
            logging.debug('Failed to open')
            self.failure.emit()


class GrblCloseWorker(QObject):
    finished = Signal()

    def __init__(self, grbl_object: GrblDevice):
        self.grbl_object = grbl_object
        super().__init__()

    def run(self):
        # logging.debug('Here!')
        self.grbl_object.disconnect()
        logging.debug('closing thread should quit now')
        self.finished.emit()


class FindDevicesWorker(QObject):
    started = Signal()
    finished = Signal(list)

    def run(self):
        logging.debug('Emitting start')
        self.started.emit()
        logging.debug('Scanning ports')
        devices = serial_ports()
        logging.debug(f'emitting {len(devices)} ports')
        self.finished.emit(devices)


class JobWorker(QObject):
    # started = Signal()
    # finished = Signal()
    next_signal = Signal(JobTuple)

    def __init__(self, path_type: str, bounds: List[Vertex],
                 steps: int, speed: float, starting_point: Vertex):
        super().__init__()
        # self.is_running = True
        self.path = PldScanPath(bounds, path_type, steps)
        self.speed = speed
        self.next_point = starting_point

    def send_next(self):
        logging.debug('Sending next!')
        self.next_point = self.path.next_point()
        self.next_signal.emit(JobTuple(vertex=self.next_point,
                                       speed=self.speed))

    @Slot(UpdateTuple)
    def update(self, update):
        x = update.x
        y = update.y
        if x is None or y is None:
            return
        r = ((self.next_point.x - x) ** 2 + (self.next_point.y - y) ** 2)**.5
        delta = r * 60 / self.speed
        # logging.debug(f'x_next={self.next_point.x}, y_next = {self.next_point.y}')
        # logging.debug(f'x={x}, y={y}')
        if delta < (UPDATE_PERIOD):
            # logging.debug(f'{delta}')
            self.send_next()