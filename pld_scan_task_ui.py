import logging

from PySide2.QtWidgets import (QGroupBox, QLabel, QDoubleSpinBox, QSpinBox,
                               QButtonGroup, QRadioButton, QStackedLayout,
                               QHBoxLayout, QVBoxLayout, QGridLayout, QWidget)
from PySide2.QtCore import Signal

from group_combo_box import GroupComboBox
from pld_scan_common import get_ds_box
from pld_scan_path import Vertex, PATH_TYPES, build_rectangle


FOUR_POINTS = 4
TWO_POINTS = 2


class TaskParametersUI(QGroupBox):
    draw_contour = Signal(list)

    def __init__(self, parent):
        super().__init__('Task Parameters', parent=parent)

        self.path_error_label = QLabel('')

        speed_label = QLabel('Speed')
        self.speed_box = QDoubleSpinBox()
        self.speed_box.setRange(.01, 999.99)
        self.speed_box.setValue(10)
        self.speed_box.setDecimals(2)
        self.speed_box.setSingleStep(.1)
        steps_label = QLabel('Steps')
        self.steps_box = QSpinBox()
        self.steps_box.setRange(1, 999)
        self.steps_box.setValue(10)
        path_type_label = QLabel('Path Type')
        self.path_type_box = GroupComboBox()
        self.populate_path_type_box()

        self.select_buttons = QButtonGroup()
        self.select_two_points = QRadioButton('Two points')
        self.select_two_points.setChecked(True)
        self.select_four_points = QRadioButton('Four points')
        self.select_buttons.addButton(self.select_two_points)
        self.select_buttons.addButton(self.select_four_points)

        self.two_points_limits = TaskParametersUI.LimitBoxes(TWO_POINTS, parent=self)
        self.current_limits = self.two_points_limits
        self.two_points_limits.draw_contour.connect(
            lambda x: self.draw_contour.emit(x)
        )
        self.four_points_limits = TaskParametersUI.LimitBoxes(FOUR_POINTS, parent=self)
        self.four_points_limits.draw_contour.connect(
            lambda x: self.draw_contour.emit(x)
        )

        self.current_box = QGroupBox('Corner points')
        current_box_layout = QStackedLayout()
        current_box_layout.addWidget(self.two_points_limits)
        current_box_layout.addWidget(self.four_points_limits)
        self.current_box.setLayout(current_box_layout)

        self.select_two_points.toggled.connect(
            self.switch_limits_layout
        )

        radio_buttons = QHBoxLayout()
        radio_buttons.addWidget(self.select_two_points)
        radio_buttons.addWidget(self.select_four_points)
        left = QVBoxLayout()
        left.addLayout(radio_buttons)
        left.addWidget(self.current_box)

        right = QGridLayout()
        right.addWidget(path_type_label, 0, 0, 1, 2)
        right.addWidget(self.path_type_box, 1, 0, 1, 2)
        right.addWidget(speed_label, 2, 0)
        right.addWidget(self.speed_box, 3, 0)
        right.addWidget(steps_label, 2, 1)
        right.addWidget(self.steps_box, 3, 1)
        right.setRowStretch(4, 1)

        lower = QHBoxLayout()
        lower.addLayout(left)
        lower.addLayout(right)

        layout = QVBoxLayout()
        layout.addWidget(self.path_error_label)
        layout.addLayout(lower)

        self.setLayout(layout)

    def switch_limits_layout(self):
        if self.select_two_points.isChecked():
            self.current_box.layout().setCurrentIndex(0)
            self.current_limits = self.two_points_limits
        elif self.select_four_points.isChecked():
            self.current_box.layout().setCurrentIndex(1)
            self.current_limits = self.four_points_limits
        self.current_limits.draw_contour_()

    def populate_path_type_box(self):
        section_closed = "CLOSED LOOPS"
        paths_closed = sorted(PATH_TYPES.CLOSED_LOOPS)
        section_open = "OPEN LOOPS"
        paths_open = sorted(PATH_TYPES.OPEN_LOOPS)
        self.path_type_box.addGroup(section_closed, paths_closed)
        self.path_type_box.addGroup(section_open, paths_open)
        self.path_type_box.setCurrentIndex(1)

    def get_contents(self):
        path_type = self.path_type_box.currentText()
        speed = self.speed_box.value()
        steps = self.steps_box.value()

        limits = self.current_box.layout().currentWidget()
        bounds = limits.get_bounds()

        return path_type, speed, steps, bounds

    def state_open(self):
        self.setEnabled(True)

    def state_closed(self):
        self.setEnabled(True)

    def state_running(self):
        self.setEnabled(False)

    class LimitBoxes(QWidget):
        draw_contour = Signal(list)

        def __init__(self, num_of_points, parent):
            super().__init__(parent=parent)
            self.num_of_points = num_of_points
            self.x = []
            self.y = []
            layout = QGridLayout()
            for i in range(num_of_points):
                x_label = QLabel(f'x{ i +1}')
                x_box = get_ds_box()
                x_box.valueChanged.connect(self.draw_contour_)
                self.x.append(x_box)

                y_label = QLabel(f'y{ i +1}')
                y_box = get_ds_box()
                y_box.valueChanged.connect(self.draw_contour_)
                self.y.append(y_box)

                layout.addWidget(x_label, i, 0)
                layout.addWidget(x_box, i, 1)
                layout.addWidget(y_label, i, 2)
                layout.addWidget(y_box, i, 3)
            layout.setRowStretch(num_of_points, 1)
            layout.setColumnStretch(4, 1)

            self.setLayout(layout)

        def get_bounds(self):
            bounds = []
            for i in range(self.num_of_points):
                x = self.x[i].value()
                y = self.y[i].value()
                bounds.append(Vertex(x, y))
            # if self.num_of_points == 2:
            #     bounds = build_rectangle(bounds[0], bounds[1])
            return bounds

        def draw_contour_(self):
            logging.debug(self.parent())
            contour = self.get_bounds()
            if self.num_of_points == 2:
                contour = build_rectangle(contour[0], contour[1])
            self.draw_contour.emit(contour)
