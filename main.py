from pld_scan_window import PldScanWindow
import sys
import logging
import time
from PySide2.QtWidgets import QApplication


if __name__ == '__main__':
    format = '%(asctime)s:%(levelname)s:%(threadName)s:%(funcName)s:%(message)s'
    logging.basicConfig(
        format=format,
        level=logging.DEBUG,
        filename='log.log',
        filemode='w',
        datefmt='%Y-%m-%d %H-%M-%S'
    )
    logging.getLogger('matplotlib.font_manager').disabled = True
    app = QApplication(sys.argv)
    # QWidget
    # widget = Widget()
    # QMainWindow using QWidget as central widget
    window = PldScanWindow()

    # Execute application
    sys.exit(app.exec_())