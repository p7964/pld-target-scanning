# import logging
from PySide2.QtWidgets import (QDoubleSpinBox,
                               QSpinBox,
                               QLabel,
                               QCheckBox,
                               QHBoxLayout,
                               QDialog,
                               QGroupBox,
                               QDialogButtonBox,
                               QGridLayout)
from PySide2.QtCore import Qt

from grbl_device import GrblDevice, CONF_KEY

X_INVERT = 0b001
Y_INVERT = 0b010
MASK_CUTOFF = 0b100


def set_ds_box(**kwargs):
    box = QDoubleSpinBox()
    box.setKeyboardTracking(False)
    box.setRange(kwargs['min'], kwargs['max'])
    # box.setSuffix(kwargs['suffix'])
    box.setDecimals(kwargs['decimals'])
    box.setSingleStep(kwargs['step'])
    box.setObjectName(kwargs['name'])
    return box


def set_is_box(**kwargs):
    box = QSpinBox()
    box.setKeyboardTracking(False)
    box.setRange(kwargs['min'], kwargs['max'])
    # box.setSuffix(kwargs['suffix'])
    box.setSingleStep(kwargs['step'])
    box.setObjectName(kwargs['name'])
    return box


GRBL_CONF_FLOATS = ('$100', '$101', '$102',
                    '$110', '$111', '$112',
                    '$120', '$121', '$122',
                    '$130', '$131', '$132')


class PldScanConfigWindow(QDialog):
    def __init__(self, parent, grbl_object: GrblDevice = None):
        super(PldScanConfigWindow, self).__init__(parent)
        self.setWindowTitle('Grbl Config')
        self.grbl_object = grbl_object

        layout = QGridLayout()
        self.input_boxes = []

        label_0 = QLabel(CONF_KEY['$0'])
        box_0 = set_is_box(name='$0', max=100, min=1, step=1)
        layout.addWidget(label_0, 0, 0, alignment=Qt.AlignRight)
        layout.addWidget(box_0, 0, 1, alignment=Qt.AlignLeft)
        self.input_boxes.append(box_0)

        label_1 = QLabel(CONF_KEY['$1'])
        box_1 = set_is_box(name='$1', max=255, min=1, step=1)
        layout.addWidget(label_1, 1, 0, alignment=Qt.AlignRight)
        layout.addWidget(box_1, 1, 1, alignment=Qt.AlignLeft)
        self.input_boxes.append(box_1)

        box_3 = PldScanConfigWindow.DirInvCB()
        layout.addWidget(box_3, 2, 0, 1, 2)
        self.input_boxes.append(box_3)

        for i, item in enumerate(GRBL_CONF_FLOATS):
            label = QLabel(CONF_KEY[item])
            box = set_ds_box(name=item,
                             min=0,
                             max=1e4,
                             step=1,
                             decimals=3)
            layout.addWidget(label, i + 3, 0, alignment=Qt.AlignRight)
            layout.addWidget(box, i + 3, 1, alignment=Qt.AlignLeft)
            self.input_boxes.append(box)

        self.populate_values()

        # scroll_area = QScrollArea()
        # scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        # scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        # scroll_area.setWidgetResizable(True)
        # scroll_area.setLayout(layout)

        ok = QDialogButtonBox.Ok
        apply = QDialogButtonBox.Apply
        cancel = QDialogButtonBox.Cancel
        butt_u = ok | apply | cancel

        buttonBox = QDialogButtonBox(butt_u)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        buttonBox.button(apply).clicked.connect(self.apply)

        # layout_full = QVBoxLayout()
        # layout_full.addWidget(scroll_area)
        # layout_full.addWidget(buttonBox)
        layout.addWidget(buttonBox, len(GRBL_CONF_FLOATS) + 4, 1,
                         alignment=Qt.AlignRight)
        self.setLayout(layout)

    def populate_values(self):
        if self.grbl_object is None:
            for item in self.input_boxes:
                item.setDisabled(True)
            return
        conf_dict = self.grbl_object.config
        for item in self.input_boxes:
            item.setEnabled(True)
            v = conf_dict[item.objectName()]
            # if isinstance(item, QDoubleSpinBox):
            #     value = float(v)
            # else:
            #     value = int(v)
            value = float(v)
            item.setValue(value)

    def accept(self):
        self.apply()
        super(PldScanConfigWindow, self).accept()

    def apply(self):
        new_conf_dict = {}
        if self.grbl_object is None:
            return
        for item in self.input_boxes:
            value = item.value()
            if isinstance(value, int):
                str_value = str(value)
            elif isinstance(value, float):
                str_value = f'{value:.3f}'
            new_conf_dict[item.objectName()] = str_value
        self.grbl_object.save_config(new_conf_dict)
        self.populate_values()

    class DirInvCB(QGroupBox):
        def __init__(self):
            self.mask = 0
            super().__init__(CONF_KEY['$3'])
            self.setAlignment(Qt.AlignCenter)
            self.setObjectName('$3')
            layout = QHBoxLayout()
            self.x_cb = QCheckBox('X')
            self.y_cb = QCheckBox('Y')
            layout.addWidget(self.x_cb)
            layout.addWidget(self.y_cb)

            self.setLayout(layout)

        def setValue(self, mask=None):
            if mask is not None:
                mask = int(mask)
                self.mask = mask
            else:
                mask = self.mask
            if mask & X_INVERT:
                self.x_cb.setChecked(True)
            else:
                self.x_cb.setChecked(False)

            if mask & Y_INVERT:
                self.y_cb.setChecked(True)
            else:
                self.y_cb.setChecked(False)

        def value(self):
            mask = self.mask
            a = mask - mask % MASK_CUTOFF
            return self.x_cb.isChecked() * X_INVERT + \
                   self.y_cb.isChecked() * Y_INVERT + a
