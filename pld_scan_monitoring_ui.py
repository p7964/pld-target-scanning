import logging

from PySide2.QtWidgets import (QGroupBox, QLabel, QGridLayout, QHBoxLayout,
                               QVBoxLayout)
from PySide2.QtCore import Signal, Slot

from pld_scan_path import Vertex
from pld_scan_common import get_ds_box, UpdateTuple
from pld_scan_graph import PldScanGraph

NONE_STRING = '       '


class MonitoringUI(QGroupBox):
    set_XY = Signal()

    def __init__(self, parent):
        super().__init__('Control and Monitoring', parent)
        self.graph = PldScanGraph()
        self.status_str = QLabel('Off'.ljust(5))
        self.error_str = QLabel()
        x_label = QLabel('x')
        y_label = QLabel('y')
        self.x_pos = QLabel(None)
        self.y_pos = QLabel(None)
        self.x_box = get_ds_box()
        self.y_box = get_ds_box()
        self.x_box.valueChanged.connect(lambda: self.set_XY.emit())
        self.y_box.valueChanged.connect(lambda: self.set_XY.emit())

        lower_layout = QGridLayout()
        lower_layout.addWidget(self.status_str, 0, 0)
        lower_layout.addWidget(self.error_str, 0, 1, 1, 2)
        lower_layout.addWidget(x_label, 1, 0)
        lower_layout.addWidget(y_label, 2, 0)
        lower_layout.addWidget(self.x_pos, 1, 1)
        lower_layout.addWidget(self.y_pos, 2, 1)
        lower_layout.addWidget(self.x_box, 1, 2)
        lower_layout.addWidget(self.y_box, 2, 2)

        upper_layout = QHBoxLayout()
        upper_layout.addStretch()
        upper_layout.addWidget(self.graph)
        upper_layout.addStretch()

        layout = QVBoxLayout()
        layout.addLayout(upper_layout)
        layout.addStretch()
        layout.addLayout(lower_layout)
        self.setLayout(layout)
        self.state_closed()

    @Slot(UpdateTuple)
    def grbl_update(self, update: UpdateTuple):
        x = update.x
        y = update.y
        status = update.status
        error = update.error
        self.x_pos.setText(NONE_STRING if x is None else f'{x:.3f}'.rjust(7))
        self.y_pos.setText(NONE_STRING if y is None else f'{y:.3f}'.rjust(7))
        self.status_str.setText(status.ljust(5))
        self.error_str.setText(error)
        if x is None or y is None:
            self.graph.move_dot(None)
        else:
            self.graph.move_dot(Vertex(x, y))

    def state_closed(self):
        self.x_box.setValue(0)
        self.y_box.setValue(0)
        self.setEnabled(False)

    def state_opened(self):
        self.setEnabled(True)

    def state_running(self):
        self.setEnabled(False)
